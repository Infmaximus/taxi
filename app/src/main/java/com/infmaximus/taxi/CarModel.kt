package com.infmaximus.taxi

data class CarModel(
    val maxSpeed: Float = 100f,
    val roundedSpeed: Float = 30f,
    val acceleration: Float = 20f,
    val braking: Float = 30f,
    val accelerationAngle: Int = 1,
    val radius: Int = 200
)