package com.infmaximus.taxi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.inflate
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val physicsHelper = PhysicsHelper(CarModel())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mainView = inflate(this, R.layout.activity_main, null)
        setContentView(mainView)

        mainView.post {
            physicsHelper.setStartPosition(mainView.width/2,mainView.height/2)
            map.setNewPosition(CarCoordinate(Position((mainView.width/2).toFloat(),(mainView.height/2).toFloat())))
        }

        map.setOnTouchListener { view, motionEvent ->
            view as CustomView
            physicsHelper.setupRoute(view, motionEvent.x,motionEvent.y)

            true
        }
    }

}
