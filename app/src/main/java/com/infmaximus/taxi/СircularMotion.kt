package com.infmaximus.taxi

import com.infmaximus.taxi.CarRotation.Companion.FULL_CIRCLE
import com.infmaximus.taxi.CarRotation.Companion.QUARTER
import com.infmaximus.taxi.CarRotation.Companion.TIME_INTERVAL
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin

class CarRotation(private val carModel: CarModel) {

    companion object {
        const val QUARTER = 90
        const val HALF_CIRCLE = 180
        const val FULL_CIRCLE = 360
        const val TIME_INTERVAL = 16
        const val SEARCH_ACCURACY = 10
    }

    private lateinit var startPosition: Position
    private lateinit var endPosition: Position
    private var angle: Float = 0f

    private var carCoordinateCircles = CarCoordinateCircles(carModel)
    private lateinit var carRotationStrategy: ICarRotation

    fun setupCircularMotion(
        startPosition: Position,
        endPosition: Position,
        angle: Float
    ) {
        this.startPosition = startPosition
        this.endPosition = endPosition
        this.angle = if (angle < 0) {
            angle + FULL_CIRCLE
        } else {
            angle
        }

        carRotationStrategy = RotationStrategyProvider()
            .getCarRotationStrategy(
                carModel,
                startPosition,
                angle,
                getDirection(),
                carCoordinateCircles
            )
    }

    fun getCarCoordinate(time: Float): CarCoordinate {
        val angle = carRotationStrategy.getCarAngle(time)
        val position = carRotationStrategy.getCarPosition(time)

        return CarCoordinate(
            position,
            angle
        )
    }
    //endregion

    fun isCarRotated(time: Float): Boolean {
        val angle = carRotationStrategy.getCarAngle(time)
        val position = carRotationStrategy.getCarPosition(time)

        val distanceBetween = abs(
            endPosition.getDistance(carCoordinateCircles.getCoordinateRightCircle(angle, position))
                    - endPosition.getDistance(carCoordinateCircles.getCoordinateLeftCircle(angle, position))
        )

        return distanceBetween > SEARCH_ACCURACY || !isCarMoviesToEnd(time)

    }

    private fun getDirection(): RotateDirection {
        val angle = if (angle < 0) {
            angle + 360
        } else {
            angle
        } % 360

        return when {
            endPosition.getDistance(carCoordinateCircles.getCoordinateLeftCircle(angle, startPosition))
                    < carModel.radius -> {
                RotateDirection.RIGHT_WITH_TURN
            }

            endPosition.getDistance(carCoordinateCircles.getCoordinateRightCircle(angle, startPosition))
                    < carModel.radius -> {
                RotateDirection.LEFT_WITH_TURN
            }

            getEndAngle() > angle && (getEndAngle() - angle < HALF_CIRCLE) -> {
                RotateDirection.LEFT
            }

            getEndAngle() < angle && ((FULL_CIRCLE + getEndAngle() - angle) < HALF_CIRCLE) -> {
                RotateDirection.LEFT
            }

            else -> {
                RotateDirection.RIGHT
            }
        }
    }

    private fun getEndAngle(): Float {
        var endAngle = atan2(startPosition.y - endPosition.y, startPosition.x - endPosition.x) /
                Math.PI * HALF_CIRCLE

        endAngle = if (endAngle < 0) {
            endAngle + FULL_CIRCLE
        } else {
            endAngle
        }

        when {
            endAngle < FULL_CIRCLE -> endAngle - QUARTER
            endAngle < FULL_CIRCLE -> endAngle - QUARTER
        }

        return (endAngle).toFloat()
    }

    /**
     * В случае поиска сложной траектории, требующей разворота, предпологаем,
     * что он нроизойдет, спустя пол периода вращения.
     */
    private fun isCarMoviesToEnd(time: Float): Boolean {
        val guaranteeTurnTime = 3000

        return when (getDirection()) {
            RotateDirection.LEFT_WITH_TURN,
            RotateDirection.RIGHT_WITH_TURN -> {
                time > guaranteeTurnTime
            }

            else -> true
        }
    }

}

interface ICarRotation {

    fun getCarPosition(time: Float): Position

    fun getCarAngle(time: Float): Float

}

class CarCoordinateCircles(private val carModel: CarModel) {

    //region RightCircle
    fun getCoordinateLeftCircle(angle: Float, position: Position): Position {
        val x = position.x + carModel.radius * cos(Math.toRadians(QUARTER - angle.toDouble())).toFloat()
        val y = position.y - carModel.radius * sin(Math.toRadians(QUARTER - angle.toDouble())).toFloat()

        return Position(x, y)
    }
    //endregion

    //region LeftCircle
    fun getCoordinateRightCircle(angle: Float, position: Position): Position {
        val x = position.x - carModel.radius * cos(Math.toRadians(QUARTER - angle.toDouble())).toFloat()
        val y = position.y + carModel.radius * sin(Math.toRadians(QUARTER - angle.toDouble())).toFloat()

        return Position(x, y)
    }
    //endregion
}

class CarLeftRotation(
    private val angle: Float,
    private val carModel: CarModel,
    private val circlePosition: Position
) : ICarRotation {
    //Для уменьшения аллокаций.
    private val position = Position()

    override fun getCarPosition(time: Float): Position {
        position.setPosition(
            getNewPositionX(time),
            getNewPositionY(time)
        )

        return position
    }

    override fun getCarAngle(time: Float): Float {
        return angle - time / TIME_INTERVAL % FULL_CIRCLE
    }

    private fun getNewPositionX(time: Float): Float {
        return circlePosition.x - (carModel.radius * sin(Math.toRadians(getCarAngle(time).toDouble())).toFloat())
    }

    private fun getNewPositionY(time: Float): Float {
        return circlePosition.y + (carModel.radius * cos(Math.toRadians(getCarAngle(time).toDouble())).toFloat())
    }

}

class CarRightRotation(
    private val angle: Float,
    private val carModel: CarModel,
    private val circlePosition: Position
) : ICarRotation {

    //Для уменьшения аллокаций.
    private val position = Position()

    override fun getCarPosition(time: Float): Position {
        position.setPosition(
            getNewPositionX(time),
            getNewPositionY(time)
        )

        return position
    }

    override fun getCarAngle(time: Float): Float {
        return angle + time / TIME_INTERVAL % FULL_CIRCLE
    }

    private fun getNewPositionX(time: Float): Float {
        return circlePosition.x + (carModel.radius * sin(Math.toRadians(getCarAngle(time).toDouble())).toFloat())
    }

    private fun getNewPositionY(time: Float): Float {
        return circlePosition.y - (carModel.radius * cos(Math.toRadians(getCarAngle(time).toDouble())).toFloat())
    }

}

class RotationStrategyProvider {

    fun getCarRotationStrategy(
        carModel: CarModel,
        startPosition: Position,
        angle: Float,
        rotateDirection: RotateDirection,
        carCoordinateCircles: CarCoordinateCircles
    ): ICarRotation {
        return (when (rotateDirection) {

            RotateDirection.RIGHT,
            RotateDirection.RIGHT_WITH_TURN -> {
                CarRightRotation(
                    angle,
                    carModel,
                    carCoordinateCircles.getCoordinateRightCircle(angle, startPosition)
                )
            }

            RotateDirection.LEFT,
            RotateDirection.LEFT_WITH_TURN -> {
                CarLeftRotation(
                    angle,
                    carModel,
                    carCoordinateCircles.getCoordinateLeftCircle(angle, startPosition)
                )
            }

        })
    }

}

enum class RotateDirection {
    RIGHT,
    RIGHT_WITH_TURN,
    LEFT,
    LEFT_WITH_TURN
}