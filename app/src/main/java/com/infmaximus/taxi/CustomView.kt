package com.infmaximus.taxi

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.graphics.*

class CustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var taxiPaint: Paint = Paint()

    private val matrixTaxi = Matrix()
    var taxi: Bitmap = BitmapFactory.decodeResource(
        context.resources,
        R.drawable.taxi_sity
    )

    var carPosition = CarCoordinate()

    fun setNewPosition(carCoordinate: CarCoordinate) {
        this.carPosition = carCoordinate

        matrixTaxi.reset()
        matrixTaxi.postRotate(
            carCoordinate.rotate,
            taxi.height.toFloat() / 2,
            taxi.width.toFloat() / 2
        )
        matrixTaxi.postTranslate(
            carCoordinate.carPosition.x - taxi.width.toFloat() / 2,
            carCoordinate.carPosition.y - taxi.height.toFloat() / 2
        )

        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.drawBitmap(taxi, matrixTaxi, taxiPaint)
    }

}
