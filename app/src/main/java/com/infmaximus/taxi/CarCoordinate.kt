package com.infmaximus.taxi

import kotlin.math.pow
import kotlin.math.sqrt

data class CarCoordinate(
    val carPosition: Position = Position(),
    val rotate: Float = 0f
)

data class Position(
    var x: Float = 0f,
    var y: Float = 0f
) {

    fun addPosition(anotherPosition: Position) {
        x += anotherPosition.x
        y += anotherPosition.y
    }

    fun setPosition(x: Float, y: Float) {
        this.x = x
        this.y = y
    }

    fun getDistance(anotherPosition: Position): Float {
        return sqrt((anotherPosition.x - this.x).pow(2) + (anotherPosition.y - this.y).pow(2))
    }

}

fun Float.getThousandths(): Float {
    return this / 1000
}