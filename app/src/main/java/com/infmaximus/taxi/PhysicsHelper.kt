package com.infmaximus.taxi

import android.os.Handler
import java.util.*
import kotlin.concurrent.timer
import android.os.Looper

class PhysicsHelper(carModel: CarModel) {

    private var carStartPosition: Position = Position()
    private var carEndPosition: Position = Position()
    private var angle: Float = 0f

    private var currentTime: Long = 0

    private var straightTrafficInitialize: Boolean = false
    private var startRoute: Boolean = false
    private var lockRotation  = false
    private var endMoving  = false

    private val carRotation: CarRotation = CarRotation(carModel)
    private val carDirectDrive: CarDirectDrive = CarDirectDrive(carModel)

    private val handler = Handler(Looper.getMainLooper())
    private var timer = Timer()

    fun setupRoute(
        view: CustomView,
        endPointX: Float,
        endPointY: Float
    ) {
        if (!startRoute) {
            startRoute = true

            carEndPosition = Position(endPointX, endPointY)

            currentTime = Date().time

            carRotation.setupCircularMotion(
                Position(carStartPosition.x, carStartPosition.y),
                Position(endPointX, endPointY),
                angle
            )

            timer =timer(period = 16L) {
                handler.post {
                    view.setNewPosition(
                        getPositionByTime(
                            Date().time - currentTime
                        )
                    )
                }
            }
        }
    }

    fun setStartPosition(
        startPointX: Int,
        startPointY: Int
    ) {
        carStartPosition = Position(
            startPointX.toFloat(),
            startPointY.toFloat()
        )
        angle = 0f
    }

    private fun getPositionByTime(time: Long): CarCoordinate {
        val carCoordinate = if (!lockRotation && carRotation.isCarRotated(time.toFloat())) {
            carRotation.getCarCoordinate(time.toFloat())
        } else if(!endMoving){
            lockRotation = true
            if(!straightTrafficInitialize){
                straightTrafficInitialize = true
                carDirectDrive.setupCarDirectRoute(carStartPosition, carEndPosition,time.toFloat(), angle)
            }
            if(carDirectDrive.isMoving(time.toFloat())){
                endMoving = true
            }

            carDirectDrive.getCarCoordinate(time.toFloat())
        } else{
            endMoving = false
            startRoute = false
            lockRotation = false
            straightTrafficInitialize = false

            timer.cancel()
            timer.purge()

            carDirectDrive.getCarCoordinate(time.toFloat())
        }

        carStartPosition = carCoordinate.carPosition
        angle = carCoordinate.rotate

        return carCoordinate
    }

}



