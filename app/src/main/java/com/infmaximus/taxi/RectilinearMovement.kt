package com.infmaximus.taxi

import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin
import kotlin.math.sqrt

class CarDirectDrive(private val carModel: CarModel) {

    private var angle: Float = 0f
    private var startTime: Float = 0f

    private lateinit var carMovement: ICarMovement

    fun setupCarDirectRoute(
        startPosition: Position,
        endPosition: Position,
        startTime: Float,
        angle: Float
    ) {
        this.angle = angle
        this.startTime = startTime

        carMovement = CarMovementProvider(
            angle,
            carModel,
            endPosition,
            startPosition
        ).getCarMovement()
    }

    fun getCarCoordinate(time: Float): CarCoordinate {
        return CarCoordinate(carMovement.getPosition((time - startTime).getThousandths()), rotate = angle)
    }

    fun isMoving(time: Float): Boolean {
        return carMovement.isTimeEnd((time - startTime).getThousandths())
    }

}

interface IAcceleratorCoordinateProvider {

    fun getAcceleratorCoordinateWithoutStart(time: Float): Position

    fun getBrakingCoordinate(time: Float): Position

}

interface IFullSegmentCoordinateProvider : IAcceleratorCoordinateProvider {

    fun getConstantSpeedCoordinate(time: Float): Position

    fun getAcceleratorCoordinate(time: Float): Position

}

class CoordinateProvider(
    private val angle: Float,
    private val carModel: CarModel
) : IFullSegmentCoordinateProvider {
    //Для уменьшения аллокаций.
    private val position = Position()

    //region CalcPosition
    override fun getAcceleratorCoordinate(time: Float): Position {
        return getPosition(carModel.roundedSpeed, carModel.acceleration, time)
    }

    override fun getConstantSpeedCoordinate(time: Float): Position {
        return getPosition(carModel.maxSpeed, time = time)
    }

    override fun getBrakingCoordinate(time: Float): Position {
        return getPosition(carModel.maxSpeed, -carModel.braking, time = time)
    }

    override fun getAcceleratorCoordinateWithoutStart(time: Float): Position {
        return getPosition(acceleration = carModel.acceleration, time = time)
    }

    //endregion

    private fun getPosition(
        startSpeed: Float = 0F,
        acceleration: Float = 0F,
        time: Float = 0F
    ): Position {
        val x = cos(Math.toRadians(angle.toDouble())) * ((startSpeed * time) + acceleration * time * time / 2)
        val y = sin(Math.toRadians(angle.toDouble())) * ((startSpeed * time) + acceleration * time * time / 2)

        position.setPosition(x.toFloat(), y.toFloat())

        return position
    }

}

/**
 * Существует 2 варианта движения.
 * 1. Когда машина достигает своей максимальной скорости.
 * 2 Когда Машина вынуждена начать тормозить не достигнув своей максимальной скорости.
 */
interface ICarMovement {

    fun isTimeEnd(time: Float): Boolean

    fun getPosition(time: Float): Position

}

class OnlyAcceleratorCarMovement(
    private val position: Position,
    private val acceleratorCoordinateProvider: IAcceleratorCoordinateProvider,

    private val timeEndAccelerator: Float,
    timeEndRoute: Float
) : ICarMovement {

    private var movementMode = MovementMode.ACCELERATOR

    private var savedPosition = Position()

    private val allTime = timeEndAccelerator + timeEndRoute

    override fun isTimeEnd(time: Float): Boolean {
        return time > allTime
    }

    override fun getPosition(time: Float): Position {
        updatePositionByTimeSegment(time)

        val deltaPosition = when (movementMode) {
            MovementMode.ACCELERATOR -> acceleratorCoordinateProvider.getAcceleratorCoordinateWithoutStart(time)

            MovementMode.BRAKE -> acceleratorCoordinateProvider.getBrakingCoordinate(
                time - timeEndAccelerator
            )

            else -> Position()
        }

        savedPosition.setPosition(position.x + deltaPosition.x, position.y + deltaPosition.y)

        return savedPosition
    }

    private fun updatePositionByTimeSegment(time: Float) {
        when (movementMode) {

            MovementMode.BRAKE -> {
                if (time > allTime) {
                    movementMode = MovementMode.END

                    position.setPosition(savedPosition.x, savedPosition.y)
                }
            }

            MovementMode.ACCELERATOR -> {
                if (time > timeEndAccelerator) {
                    movementMode = MovementMode.BRAKE

                    position.setPosition(savedPosition.x, savedPosition.y)
                }
            }

            else -> {
            }
        }
    }

}

class WithConstantSpeedCarMovement(
    private val position: Position,
    private val fullSegmentCoordinateProvider: IFullSegmentCoordinateProvider,

    private val timeEndAccelerator: Float,
    private val timeEndConstantSpeed: Float,
    timeEndRoute: Float
) : ICarMovement {

    private var movementMode = MovementMode.ACCELERATOR

    private var savedPosition = Position()

    private val allTime = timeEndConstantSpeed + timeEndAccelerator + timeEndRoute

    override fun isTimeEnd(time: Float): Boolean {
        return time > allTime
    }

    override fun getPosition(time: Float): Position {
        updatePositionByTimeSegment(time)

        val deltaPosition = when (movementMode) {
            MovementMode.ACCELERATOR -> fullSegmentCoordinateProvider.getAcceleratorCoordinate(time)

            MovementMode.SPEED -> fullSegmentCoordinateProvider.getConstantSpeedCoordinate(time - timeEndAccelerator)

            MovementMode.BRAKE -> fullSegmentCoordinateProvider.getBrakingCoordinate(
                time - timeEndAccelerator - timeEndConstantSpeed
            )

            MovementMode.END -> Position()
        }

        savedPosition.setPosition(position.x + deltaPosition.x, position.y + deltaPosition.y)

        return savedPosition
    }

    private fun updatePositionByTimeSegment(time: Float) {
        when (movementMode) {
            MovementMode.END -> {
            }

            MovementMode.BRAKE -> {
                if (time > allTime) {
                    movementMode = MovementMode.END

                    position.setPosition(savedPosition.x, savedPosition.y)
                }
            }

            MovementMode.SPEED -> {
                if (time > timeEndAccelerator + timeEndConstantSpeed) {
                    movementMode = MovementMode.BRAKE

                    position.setPosition(savedPosition.x, savedPosition.y)
                }
            }

            MovementMode.ACCELERATOR -> {
                if (time > timeEndAccelerator) {
                    movementMode = MovementMode.SPEED

                    position.setPosition(savedPosition.x, savedPosition.y)
                }
            }
        }
    }

}

class CarMovementProvider(
    angle: Float,
    private val carModel: CarModel,
    private var endPosition: Position,
    private var startPosition: Position
) {

    private val coordinateProvider = CoordinateProvider(angle, carModel)

    //region Distance
    fun getCarMovement(): ICarMovement {
        return if (startPosition.getDistance(endPosition) >=
            getDirectDirectionByAccelerator() + getDirectDirectionByBraking()
        ) {
            WithConstantSpeedCarMovement(
                startPosition,
                coordinateProvider,
                getTimeAcceleratorToConstantSpeed(),
                getTimeConstantSpeedToBraking(),
                getTimeBrakingToEnd()
            )
        } else {
            OnlyAcceleratorCarMovement(
                startPosition,
                coordinateProvider,
                getTimeAcceleratorToBraking(),
                getTimeBrakingAfterAcceleratorToEnd()
            )
        }
    }

    private fun getDirectDirectionByAccelerator(): Float {
        return (carModel.maxSpeed.pow(2) - carModel.roundedSpeed.pow(2)) / (2 * carModel.acceleration)
    }

    private fun getDirectDirectionByBraking(): Float {
        return (carModel.maxSpeed.pow(2)) / (2 * carModel.braking)
    }
    //endregion

    //region TimeDoubleSegment
    /**
     * Попробовал порешать слау, но если честно под вечер не до конца разобрался, где ошибка.
     * Из за этого начальная скорость в 2х сегментном разгоне не учитывается.
     * Поэтому решение слау без начальной скорости.
     */
    private fun getTimeAcceleratorToBraking(): Float {
        return sqrt(2 * carModel.braking * startPosition.getDistance(endPosition) /
                (carModel.acceleration * carModel.braking + carModel.acceleration * carModel.acceleration))

/*        val a = carModel.acceleration+carModel.braking
        val b = 4*carModel.roundedSpeed
        val c = carModel.roundedSpeed.pow(2)/carModel.braking-2*startPosition.getDistance(endPosition)

        val d = b.pow(2) - 4*a*c

        val time = (-b+ sqrt(d))/(2*a)

        return time*/
    }

    private fun getTimeBrakingAfterAcceleratorToEnd(): Float {
        return carModel.acceleration * getTimeAcceleratorToBraking() / carModel.braking
    }
    //endregion

    //region TimeTripleSegment
    private fun getTimeAcceleratorToConstantSpeed(): Float {
        return (carModel.maxSpeed - carModel.roundedSpeed) / carModel.acceleration
    }

    private fun getTimeConstantSpeedToBraking(): Float {
        return (startPosition.getDistance(endPosition) -
                (getDirectDirectionByAccelerator() + getDirectDirectionByBraking())) /
                (carModel.maxSpeed)
    }

    private fun getTimeBrakingToEnd(): Float {
        return carModel.maxSpeed / carModel.braking
    }
    //endregion
}

enum class MovementMode {
    ACCELERATOR,
    SPEED,
    BRAKE,
    END
}